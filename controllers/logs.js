var Logs = require('../models/logs');

exports.get= function(req, res){
    Logs.get(req,function(err, logs){
        if(err){
          return  res.sendStatus(500);
        }
        res.send(logs);
    })
}

exports.save= function(req, res){
    Logs.save(req,function(err,value){
        if(err){
          return  res.sendStatus(500);
        }else{
            res.send(value);
        }
    })
}
