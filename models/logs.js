var mongoose=require('mongoose');
var Joi=require('joi');
var lSchema=Joi.object().keys({
    title:Joi.string().required(),
    type:Joi.string().required(),
    details:Joi.string().required(),
    created:Joi.date()
})
mongoose.Promise = global.Promise;
var Schema=mongoose.Schema;

var logSchema=new Schema({
    title:String,
    type:String,
    details:String,
    created:Date
})



Logs=mongoose.model('logs',logSchema);


exports.get=function(req , cb){
        mongoose.model('logs').find(req.query, function(err, logs){
            cb(err, logs)
        });
}

exports.save=function(req,cb){
        var log = {
            title:req.body.title,
            type:req.body.type,
            details:req.body.details,
            created:Date.now()
        };
        const result=Joi.validate(log, lSchema, function(err,value){
            if(!err){
                var newLog=new Logs(value);
                newLog.save();
                callBack();
            }else{
                callBack();
            }
           function callBack(){
                cb(err,value)
            }
        });  
    }
