var express = require('express');
var bodyParser=require('body-parser');
var config= require('./config')
var mongoose = require('mongoose');
var logsController=require('./controllers/logs');
var app=express();

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());


app.get('/logs', logsController.get)
app.post('/logs',logsController.save)
   

mongoose.connect(config.get("mongoose:uri"));
var port=config.get("port") || 3000;
app.listen(port, ()=> console.log(`Listen on port ${port}`));
